"""
========================================================
Contoh Penggunaan Library untuk Garuda L76G
========================================================
Nama Program : Loop Program Class A
Versi        : v0.1.0

Author       : Ardianto, A.I.
https://lunar-inovasi.co.id/
========================================================
Penjelasan:

Dengan memanfaatkan Garuda L76G sebagai modem LoRa,
perangkat akan disetel ke kelas A sebagai pengirim data
ke server. Program berjalan pada loop tak hingga dengan
periode tertentu.
========================================================
"""

from garudal76g import *

""" Pengaturan Join Mode """
JOIN_MODE   = OTAA              # Pilihan: OTAA, ABP

""" === Pengaturan Jaringan LoRa === """
""" Pengaturan Adaptive Data Rate """
ADR_STATUS  = ON                # Pilihan: ON, OFF
""" Jika ADR OFF, perlu diatur Data Rate dan Tx Power """
DATA_RATE   = 2                 # Pilihan: 0-5
TX_POWER    = 16                # Pilihan: 0-16 (dBm), bernilai genap
""" Pengaturan Port """
LORAWAN_PORT= 1                 # Pilihan: 1-255, selain 224, 240
""" Pengaturan Tx Confirm """
TX_CONFIRM  = ON                # Pilihan: ON, OFF
""" Pengaturan Debug Mode """
DEBUG_MODE  = ON                # Pilihan: ON, OFF
""" Pengaturan Mode Input Pengiriman Data """
INPUT_MODE  = AUTO              # Pilihan: AUTO, MANUAL

""" Pengaturan Secure Element Identity """

""" Jika menggunakan join mode OTAA """
DEV_EUI     = "00a1b2c3d4e5f600"
APP_EUI     = "0000000000000000"
APP_KEY     = "0afb313ccff7ccdc2fc5f7b55d815cba"

""" Jika menggunakan join mode ABP """
DEV_ADDR    = "00000000"
NWKS_KEY    = "00000000000000000000000000000000"
APPS_KEY    = "00000000000000000000000000000000"

""" Instatiasi Object """
GarudaShield = GarudaL76G()

""" Terapkan Pengaturan ke Modem """
GarudaShield.loraClass  = CLASS_A
GarudaShield.joinMode   = JOIN_MODE
GarudaShield.ADR        = ADR_STATUS
GarudaShield.dataRate   = DATA_RATE
GarudaShield.TxPower    = TX_POWER
GarudaShield.TxConfirm  = TX_CONFIRM
GarudaShield.debugMode  = DEBUG_MODE

if JOIN_MODE == OTAA:
    GarudaShield.initOTAA(DEV_EUI, APP_EUI, APP_KEY)
elif JOIN_MODE == ABP:
    GarudaShield.initABP(DEV_ADDR, NWKS_KEY, APPS_KEY)

""" Tampilkan Pengaturan """
print("===== Garuda L76G - Class A Program Example =====")
print("Firmware Version:", GarudaShield.firmwareVersion)
print("=================================================")
print("Join Mode    :", printCode(GarudaShield.joinMode))
print("ADR Status   :", printCode(GarudaShield.ADR))
if GarudaShield.ADR == OFF:
    print("Data Rate    :", GarudaShield.dataRate)
    print("Tx Power     :", GarudaShield.TxPower)
print("Tx Confirm   :", printCode(GarudaShield.TxConfirm))
print("Debug Mode   :", printCode(GarudaShield.debugMode))
if GarudaShield.joinMode == OTAA:
    print("DevEUI       :", GarudaShield.DevEUI)
    print("AppEUI       :", GarudaShield.AppEUI)
    print("AppKey       :", GarudaShield.AppKey)
elif GarudaShield.joinMode == ABP:
    print("DevAddr      :", GarudaShield.DevAddr)
    print("NwkSKey      :", GarudaShield.NwkSKey)
    print("AppSKey      :", GarudaShield.AppSKey)
else:
    print("Mode tidak terdeteksi")
print("=================================================")

""" Join ke jaringan hingga berhasil. """
print("Join ke jaringan...")
join_status = GarudaShield.join()
while join_status not in (JOIN_OTAA_SUCCESS, JOIN_ABP_SUCCESS):
    join_status = GarudaShield.join()
    print("Join Gagal (" + str(join_status) + ").")
    print("Mencoba untuk join kembali...")
    time.sleep(1)
print("Join berhasil!")
print("=================================================")

""" Variabel untuk Program Utama """
""" Payload tipe data pengiriman dapat berupa integer,
string, atau keduanya. Tiap payload dipisahkan dengan
tanda koma. Pada input data secara manual, tiap payload
dipisahkan dengan spasi. """

delay_sleep = 200
payload_data = ''
payload_stat = 0

""" Loop Utama """
""" Payload untuk dikirim akan di-encode ke format hex """
while(1):
    if INPUT_MODE == AUTO:
        payload_data  = (0,1,2,"Hello!")
        print("Mengirim...")
        payload_stat = GarudaShield.sendData(0,1,2,"Hello!")
    if INPUT_MODE == MANUAL:

        """ Kirim payload bertipe string """
        # payload_data = input("Payload untuk dikirim (str): ")
        # print("Mengirim...")
        # GarudaShield.sendData(payload_data)

        """ Kirim payload bertipe integer """
        # try:
        #     payload_data = int(input("Payload untuk dikirim (int): "))
        #     print("Mengirim...")
        #     GarudaShield.sendData(payload_data)
        # except:
        #     print("Data harus bertipe integer!")

        """ Kirim payload bertipe hex """
        payload_data = input("Payload untuk dikirim (hex): ")
        print("Mengirim...")
        payload_stat = GarudaShield.sendData(payload_data, isHex = True)

        """ Kirim lebih dari satu payload integer dan string """
        # data_input = input("Payload untuk dikirim (pisahkan dengan spasi):\n")
        # payload_data = data_input.split()
        # elem_pos = 0
        # for elem in payload_data:
        #     try:
        #         payload_data[elem_pos] = int(elem)
        #     except:
        #         pass
        #     elem_pos += 1
        # print("Mengirim...")
        # payload_stat = GarudaShield.sendData(*payload_data)
    
    if payload_stat is DATA_SENT:
        print("Data terkirim!")
        print("Port             :", LORAWAN_PORT)
        print("Payload dikirim  :", payload_data)
        print("Payload hex      :", GarudaShield.sendResponse.payload)
        print("Tx Counter       :", GarudaShield.sendResponse.txCounter)
        print("Tx Data Rate     :", GarudaShield.sendResponse.txDataRate)
        print("RSSI             :", GarudaShield.sendResponse.rssi)
        print("SNR              :", GarudaShield.sendResponse.snr)
        print("Rx Counter       :", GarudaShield.sendResponse.rxCounter)
        print("Rx Data Rate     :", GarudaShield.sendResponse.rxDataRate)
    else:
        print("Data gagal terkirim (" + str(payload_stat) + ").")
    print("=================================================")
    time.sleep(2)