from event_code import *

RAISE_ERROR = ON

class ErrorHandling():
    def __init__(self) -> None:
        pass
    def userWarning(self, error_message):
        if RAISE_ERROR == ON:
            raise UserWarning(error_message)
        else:
            print("Error ccured: " + error_message)
    def runtimeError(self, error_message):
        if RAISE_ERROR == ON:
            raise RuntimeError(error_message)
        else:
            print("Error ccured: " + error_message)
    def typeError(self, param_name, data_type):
        error_message = param_name + " must be " + data_type + " data type!"
        if RAISE_ERROR == ON:
            raise TypeError(error_message)
        else:
            print("Error ccured: " + error_message)
    def digitError(self, param_name, digit):
        error_message = param_name + " length must be " + str(digit) + " digits!"
        if RAISE_ERROR == ON:
            raise ValueError(error_message)
        else:
            print("Error ccured: " + error_message)
    def rangeError(self, param_name, range_min, range_max, unit = ''):
        error_message = param_name + " must be between " + str(range_min) + " to " + str(range_max) + ' ' + unit + "!"
        if RAISE_ERROR == ON:
            raise ValueError(error_message)
        else:
            print("Error ccured: " + error_message)
    def selectionError(self, param_name, selection):
        error_message = param_name + " selection must be " + selection + '!'
        if RAISE_ERROR == ON:
            raise TypeError(error_message)
        else:
            print("Error ccured: " + error_message)

if __name__ == "__main__":
    GarudaError = ErrorHandling()
    for key, value in code_dict.items():
        print("Key:", key, '| Value:', value)
    GarudaError.userWarning("This is a warning")
    GarudaError.rangeError("Data rate", 0, 5)