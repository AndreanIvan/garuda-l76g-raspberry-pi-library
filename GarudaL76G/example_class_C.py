"""
========================================================
Contoh Penggunaan Library untuk Garuda L76G
========================================================
Nama Program : Loop Program Class C
Versi        : v0.1.0

Author       : Ardianto, A.I.
https://lunar-inovasi.co.id/
========================================================
Penjelasan:

Dengan memanfaatkan Garuda L76G sebagai modem LoRa,
perangkat akan disetel ke kelas C sebagai penerima data
dari server. Program berjalan pada loop tak hingga dengan
periode tertentu.
========================================================
"""

from garudal76g import *

""" Pengaturan Join Mode """
JOIN_MODE   = OTAA              # Pilihan: OTAA, ABP

""" === Pengaturan Jaringan LoRa === """
""" Pengaturan Adaptive Data Rate """
ADR_STATUS  = ON                # Pilihan: ON, OFF
""" Jika ADR OFF, perlu diatur Data Rate dan Tx Power """
DATA_RATE   = 2                 # Pilihan: 0-5
TX_POWER    = 16                # Pilihan: 0-16 (dBm), bernilai genap
""" Pengaturan Port """
LORAWAN_PORT= 1                 # Pilihan: 1-255, selain 224, 240
""" Pengaturan Tx Confirm """
TX_CONFIRM  = ON                # Pilihan: ON, OFF
""" Pengaturan Debug Mode """
DEBUG_MODE  = ON                # Pilihan: ON, OFF

""" Pengaturan Secure Element Identity """

""" Jika menggunakan join mode OTAA """
DEV_EUI     = "00a1b2c3d4e5f600"
APP_EUI     = "0000000000000000"
APP_KEY     = "0afb313ccff7ccdc2fc5f7b55d815cba"

""" Jika menggunakan join mode ABP """
DEV_ADDR    = "00000000"
NWKS_KEY    = "00000000000000000000000000000000"
APPS_KEY    = "00000000000000000000000000000000"

""" Instatiasi Object """
GarudaShield = GarudaL76G()

""" Terapkan Pengaturan ke Modem """
GarudaShield.loraClass  = CLASS_C
GarudaShield.joinMode   = JOIN_MODE
GarudaShield.ADR        = ADR_STATUS
GarudaShield.dataRate   = DATA_RATE
GarudaShield.TxPower    = TX_POWER
GarudaShield.TxConfirm  = TX_CONFIRM
GarudaShield.debugMode  = DEBUG_MODE

if JOIN_MODE == OTAA:
    GarudaShield.initOTAA(DEV_EUI, APP_EUI, APP_KEY)
elif JOIN_MODE == ABP:
    GarudaShield.initABP(DEV_ADDR, NWKS_KEY, APPS_KEY)

""" Tampilkan Pengaturan """
print("===== Garuda L76G - Example Class C Program ===== ")
print("Firmware Version:", GarudaShield.firmwareVersion)
print("=================================================")
print("Join Mode    :", printCode(GarudaShield.joinMode))
print("ADR Status   :", printCode(GarudaShield.ADR))
if GarudaShield.ADR == OFF:
    print("Data Rate    :", GarudaShield.dataRate)
    print("Tx Power     :", GarudaShield.TxPower)
print("Tx Confirm   :", printCode(GarudaShield.TxConfirm))
if GarudaShield.joinMode == OTAA:
    print("DevEUI       :", GarudaShield.DevEUI)
    print("AppEUI       :", GarudaShield.AppEUI)
    print("AppKey       :", GarudaShield.AppKey)
elif GarudaShield.joinMode == ABP:
    print("DevAddr      :", GarudaShield.DevAddr)
    print("NwkSKey      :", GarudaShield.NwkSKey)
    print("AppSKey      :", GarudaShield.AppSKey)
else:
    print("Mode tidak terdeteksi")
print("=================================================")

""" Join ke jaringan hingga berhasil. """
print("Join ke jaringan...")
join_status = GarudaShield.join()
while join_status not in (JOIN_OTAA_SUCCESS, JOIN_ABP_SUCCESS):
    join_status = GarudaShield.join()
    print("Join Gagal (" + str(join_status) + ").")
    print("Mencoba untuk join kembali...")
    time.sleep(1)
print("Join berhasil!")
print("=================================================")


""" Server Chirptack mengharuskan adanya minimal 2 kali uplink
sebelum dilakukan downlink """
send_count = 0
while send_count < 2 :
    if GarudaShield.sendData(0) is DATA_SENT:
        send_count += 1

""" Rx Window akan selalu terbuka dan device berada pada
kondisi menunggu downlink dari server. """

while(1):
    print("Menunggu data...")
    time.sleep(0.5)
    GarudaShield.receiveData()
    print("Port      :", GarudaShield.receivedData.port)
    print("Payload   :", GarudaShield.receivedData.payload)
    print("Counter   :", GarudaShield.receivedData.counter)
    print("Data rate :", GarudaShield.receivedData.data_rate)
    print("RSSI      :", GarudaShield.receivedData.rssi, "dBm")
    print("SNR       :", GarudaShield.receivedData.snr, "dB")
    print("=================================================")