from error_handling import *
import serial
import time

""" Log Setting """
PROCESS_LOG = OFF
ERROR_LOG   = OFF
UART_LOG    = OFF
LORA_LOG    = OFF

GarudaError = ErrorHandling()

class GarudaL76G:
    def __init__(self):
        self.Garuda_Serial = serial.Serial(
            port = '/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
            baudrate = 115200,
            parity = serial.PARITY_NONE,
            stopbits = serial.STOPBITS_ONE,
            bytesize = serial.EIGHTBITS,
            timeout = 1
        )
        self.Garuda_Serial.reset_input_buffer()
        self.Garuda_Serial.reset_output_buffer()
        self._joinMode  = -1
        self._TxConfirm = -1
        self._DevAddr   = ""
        self._DevEUI    = ""
        self._AppEUI    = ""
        self._NwkSKey   = ""
        self._AppKey    = ""
        self._AppSKey   = ""
        self._MCAddr    = ""
        self._MCNwkSKey = ""
        self._MCAppSKey = ""
        

    """ Log Print """

    def processPrint(self, strPrint):
        if PROCESS_LOG:
            print(strPrint)

    def errorPrint(self, strPrint):
        if ERROR_LOG or PROCESS_LOG:
            print(strPrint)

    def UARTPrint(self, strPrint):
        if UART_LOG:
            print(strPrint, end = '')
    
    def LoRaPrint(self, strPrint):
        if LORA_LOG:
            print(strPrint, end = '')

    """ Library Control Function """

    def sendCommand(self, *args):
        self.Garuda_Serial.reset_input_buffer()
        dataString = ''
        for arg in args:
            dataString += str(arg)
            if arg != args[len(args) - 1]:
                dataString += ' '
            
        dataString = 'mac ' + dataString + '\r\n'
        self.Garuda_Serial.write(dataString.encode())
        self.UARTPrint(dataString)

    def readString(self):
        buff = ''
        stringRead = ""
        buff = self.Garuda_Serial.readline()
        stringRead += buff.decode()
        self.UARTPrint(stringRead)
        return stringRead

    def readUntil(self, string = "", timeout = 1):
        stringRead = ""

        # If there's no argument, read until input buffer is empty
        if string is "":
            time.sleep(0.1)
            while self.Garuda_Serial.in_waiting > 0 :
                buff = self.Garuda_Serial.readline()
                stringRead += buff.decode()

        # If timeout is set to None, read until string found only, regardless the timeout
        else:
            isFound = False
            if timeout != None :
                timeout += time.time()
            while not isFound:
                buff = self.Garuda_Serial.readline()
                stringRead += buff.decode()
                if string in stringRead:
                    isFound = True
                if timeout != None and time.time() > timeout:
                    self.errorPrint("Read timeout!")
                    self.UARTPrint(stringRead)
                    return stringRead
        self.UARTPrint(stringRead)
        return stringRead
    
    """ General Command """
    def info(self):
        self.sendCommand("get_info")
        return self.readUntil()

    @property
    def debugMode(self):
        self.sendCommand("get_debug")
        debug_stat = self.readString()
        if "on" in debug_stat :
            return ON
        if "off" in debug_stat :
            return OFF

    @debugMode.setter
    def debugMode(self, DebugStatus):
        if DebugStatus == ON :
            self.sendCommand("set_debug on")
        elif DebugStatus == OFF :
            self.sendCommand("set_debug off")
        else :
            GarudaError.selectionError("Debug state", "ON/OFF")
            return WRONG_PARAMETER
        self.readUntil("ok")
        return OK
    
    """ Device Command """

    def factoryReset(self):
        self.sendCommand("factory_reset")
        self.readUntil("ok")
        return OK
    
    def sleep(self):
        self.sendCommand("sleep")
        self.readUntil("ok")
        return OK
    
    def wakeUp(self):
        self.sendCommand()
        self.readUntil("waked up")
        return OK

    @property
    def hardwareDevEUI(self):
        self.sendCommand("get_hwdeveui")
        return self.readString()

    @property
    def firmwareVersion(self):
        self.sendCommand("get_ver")
        return self.readString().strip()

    """ Keys, IDs, and EUIs Management """

    @property
    def DevEUI(self):
        self.sendCommand("get_deveui")
        self._DevEUI = self.readString()
        return self._DevEUI.strip()
    @DevEUI.setter
    def DevEUI(self, deveui):
        if type(deveui) != str:
            GarudaError.typeError("Device EUI", "string")
        elif len(deveui) != 16:
            GarudaError.digitError("Device EUI", 16)
        else:
            self._DevEUI = deveui
            self.sendCommand("set_deveui " + deveui)
            self.readUntil("ok")
        return OK

    @property
    def AppEUI(self):
        self.sendCommand("get_appeui")
        self._AppEUI = self.readString()
        return self._AppEUI.strip()
    @AppEUI.setter
    def AppEUI(self, appeui):
        if type(appeui) != str:
            GarudaError.typeError("Application EUI", "string")
        elif len(appeui) != 16:
            GarudaError.digitError("Application EUI", 16)
        else:
            self._AppEUI = appeui
            self.sendCommand("set_appeui " + appeui)
            self.readUntil("ok")
        return OK

    @property
    def AppKey(self):
        self.sendCommand("get_appkey")
        self._AppKey = self.readString()
        return self._AppKey.strip()
    @AppKey.setter
    def AppKey(self, appkey):
        if type(appkey) != str:
            GarudaError.typeError("Application EUI", "string")
        elif len(appkey) != 32:
            GarudaError.digitError("Application EUI", 32)
        else:
            self._AppKey = appkey
            self.sendCommand("set_appkey " + appkey)
            self.readUntil("ok")
        return OK

    @property
    def DevAddr(self):
        self.sendCommand("get_devaddr")
        self._DevAddr = self.readString()
        return self._DevAddr
    @DevAddr.setter
    def DevAddr(self, devaddr):
        if type(devaddr) != str:
            GarudaError.typeError("Device Address", "string")
        elif len(devaddr) != 8:
            GarudaError.digitError("Device Address", 8)
        else:
            self._DevAddr = devaddr
            self.sendCommand("set_devaddr" + devaddr)
            self.readUntil("ok")
        return OK

    @property
    def NwkSKey(self):
        self.sendCommand("get_nwkskey")
        self._NwkSKey = self.readString()
        return self._NwkSKey
    @NwkSKey.setter
    def NwkSKey(self, nwkskey):
        if type(nwkskey) != str:
            GarudaError.typeError("Network Session Key", "string")
        elif len(nwkskey) != 32:
            GarudaError.digitError("Network Session Key", 32)
        else:
            self._NwkSKey = nwkskey
            self.sendCommand("set_nwkskey" + nwkskey)
            self.readUntil("ok")
        return OK

    @property
    def AppSKey(self):
        self.sendCommand("get_appskey")
        self._AppSKey = self.readString()
        return self._AppSKey
    @AppSKey.setter
    def AppSKey(self, appskey):
        if type(appskey) != str:
            GarudaError.typeError("Application Session Key", "string")
        elif len(appskey) != 32:
            GarudaError.digitError("Application Session Key", 32)
        else:
            self._AppSKey = appskey
            self.sendCommand("set_appskey" + appskey)
            self.readUntil("ok")
        return OK

    @property
    def MCAddr(self):
        self.sendCommand("get_mcaddr")
        self._MCAddr = self.readString()
        return self._MCAddr
    @MCAddr.setter
    def MCAddr(self, mcaddr):
        if type(mcaddr) != str:
            GarudaError.typeError("Multicast Address", "string")
        elif len(mcaddr) != 8:
            GarudaError.digitError("Multicast Address", 8)
        else:
            self._MCAddr = mcaddr
            self.sendCommand("set_mcaddr" + mcaddr)
            self.readUntil("ok")
        return OK

    @property
    def MCNwkSKey(self):
        self.sendCommand("get_mcnwkskey")
        self._MCNwkSKey = self.readString()
        return self._MCNwkSKey
    @MCNwkSKey.setter
    def MCNwkSKey(self, mcnwkskey):
        if type(mcnwkskey) != str:
            GarudaError.typeError("Multicast Network Session Key", "string")
        elif len(mcnwkskey) != 32:
            GarudaError.digitError("Multicast Network Session Key", 32)
        else:
            self._MCNwkSKey = mcnwkskey
            self.sendCommand("set_mcnwkskey" + mcnwkskey)
            self.readUntil("ok")
        return OK

    @property
    def MCAppSKey(self):
        self.sendCommand("get_mcappskey")
        self._MCAppSKey = self.readString()
        return self._MCAppSKey
    @MCAppSKey.setter
    def MCAppSKey(self, mcappskey):
        if type(mcappskey) != str:
            GarudaError.typeError("Multicast Application Session Key", "string")
        elif len(mcappskey) != 32:
            GarudaError.digitError("Multicast Application Session Key", 32)
        else:
            self._MCAppSKey = mcappskey
            self.sendCommand("set_mcappskey" + mcappskey)
            self.readUntil("ok")
        return OK

    def initOTAA(self,
                deveui = '0' * 16,
                appeui = '0' * 16,
                appkey = '0' * 32):
        self.DevEUI = deveui
        self.AppEUI = appeui
        self.AppKey = appkey
        return OK

    def initABP(self,
                devaddr = '0' * 8,
                nwkskey = '0' * 32,
                appskey = '0' * 32):
        self.DevAddr = devaddr
        self.NwkSKey = nwkskey
        self.AppSKey = appskey
        return OK
    
    """ Joining, Sending, and Receiving Data on LoRa Network """

    def join(self):
        self.sendCommand("join")
        join_stat = NETWORK_NOT_JOINED
        join_timeout = time.time() + 20
        
        while join_stat not in (JOIN_OTAA_SUCCESS, JOIN_ABP_SUCCESS, JOIN_FAILED, JOIN_TIMED_OUT):
            join_resp = self.readString()
            if "otaa joined" in join_resp :
                self.processPrint("Joined by OTAA.")
                join_stat = JOIN_OTAA_SUCCESS
            elif "abp joined" in join_resp :
                self.processPrint("Joined by ABP.")
                join_stat = JOIN_ABP_SUCCESS
            elif "join_fail" in join_resp :
                self.processPrint("Join failed.")
                join_stat = JOIN_FAILED
            elif time.time() > join_timeout:
                self.errorPrint("Join timed out") 
                join_stat = JOIN_TIMED_OUT
        
        return join_stat
    
    def joinStatus(self):
        self.sendCommand("get_nwksstatus")
        status = self.readString()
        if "otaa joined" in status :
            self.processPrint("Joined by OTAA.")
            return JOIN_OTAA_SUCCESS
        elif "abp joined" in status :
            self.processPrint("Joined by ABP")
            return JOIN_ABP_SUCCESS
        elif "no network" in status :
            self.processPrint("Not joined yet!")
            return NETWORK_NOT_JOINED
        else:
            self.errorPrint("Wrong response from device:")
            self.errorPrint(status)
            return GENERAL_ERROR

    @property
    def joinMode(self):
        self.sendCommand("get_mode")
        mode = self.readString()
        if "otaa" in mode : 
            self._joinMode = OTAA
        elif "abp" in mode :
            self._joinMode = ABP
        else :
            self.errorPrint("Join mode not detected!")
            return GENERAL_ERROR
        return self._joinMode
    @joinMode.setter
    def joinMode(self, joinMode):
        if joinMode == OTAA :
            self._joinMode = OTAA
            self.sendCommand("set_mode otaa")
        elif joinMode == ABP :
            self._joinMode = ABP
            self.sendCommand("set_mode abp")
        else :
            print("Join mode selection must be OTAA or ABP!")
            return WRONG_PARAMETER
        self.readUntil("ok")
        return OK

    @property
    def port(self):
        self.sendCommand("get_port")
        return self.readString()
    @port.setter
    def port(self, port_id):
        if port < 1 or port > 255 :
            GarudaError.rangeError("Selected port", 1, 255)
        else:
            self.sendCommand("set_port ", port_id)
            self.readUntil('ok')
        return OK

    @property
    def TxConfirm(self):
        self.sendCommand("get_txconfirm")
        confirmStatus = self.readString()
        if "unconfirmed" in confirmStatus:
            self._TxConfirm = OFF
        else:
            self._TxConfirm = ON
        return self._TxConfirm
    @TxConfirm.setter
    def TxConfirm(self, confirmStatus):
        if confirmStatus == ON :
            self._TxConfirm = ON
            self.sendCommand("set_txconfirm on")
        elif confirmStatus == OFF :
            self._TxConfirm = OFF
            self.sendCommand("set_txconfirm off")
        else :
            GarudaError.selectionError("TX confirm state", "ON/OFF")
            return WRONG_PARAMETER
        self.readUntil("ok")
        return OK 
    
    @property
    def lastRSSI(self):
        self.sendCommand("get_rssi")
        return int(self.readString())
    
    @property
    def lastSNR(self):
        self.sendCommand("get_snr")
        return int(self.readString())
    
    # If the data type is string it will be converted from ASCII to HEX
    # If the data type is integer it will be converted from INT to HEX
    # Support multiple arguments, the order is determined based on positional argument
    def sendData(self, *args, isHex = False):
        send_payload = ""

        if self._TxConfirm is -1:
            self._TxConfirm = self.TxConfirm

        for elem in args:
            if isHex == True:
                send_payload += elem
            elif type(elem) == int:
                hexnum = str(hex(elem))[2:]
                if len(hexnum) % 2 != 0: hexnum = '0' + hexnum
                send_payload += hexnum
            elif type(elem) == str:
                for letter in elem:
                    strhex = hex(ord(letter)).lstrip('0x')
                    send_payload += strhex
        
        self.sendCommand("send", send_payload)

        if self._TxConfirm is ON :
            send_resp = self.readUntil("rx ok", 5)
            self.sendResponse.stringParsing(send_resp)
            if send_resp == READ_TIMED_OUT:
                self.LoRaPrint("Send response timed out!\r\n")
                return SEND_RESP_TIMED_OUT
            else:
                self.LoRaPrint(send_resp)
                self.processPrint("Data sent with confirmation!")
                return DATA_SENT
        elif self._TxConfirm is OFF :
            send_resp = self.readUntil("tx ok", 5)
            self.sendResponse.stringParsing(send_resp)
            if send_resp == READ_TIMED_OUT:
                self.LoRaPrint("Send response timed out!\r\n")
                return SEND_RESP_TIMED_OUT
            else:
                self.LoRaPrint(send_resp)
                self.processPrint("Data sent without confirmation!")
                return DATA_SENT
        else:
            self.errorPrint("Tx confirm not detected!")
            return GENERAL_ERROR

    class SendResponseClass:
        def __init__(self):
            self._parsed_string = ''

        def stringParsing(self, string):
            self._parsed_string = string.split()
            self.data()
            return OK
        
        def _paramValue(self, param, debug=False, confirmed=False, start=0, units=''):
            try:
                if type(param) == int:
                    return self._parsed_string[param] + units
                else:
                    index = self._parsed_string.index(param) + 1
                    return self._parsed_string[index] + units
            except:
                if debug:
                    if confirmed:
                        return "Not detected. Debug mode and Tx confirm status must be ON."
                    return "Not detected. Debug mode must be ON."
                else:
                    return '-'

        def data(self):
            self.port = self._paramValue(2)
            self.payload = self._paramValue(3)
            self.timeOnAir = self._paramValue('air', True)
            self.txCounter = self._paramValue('cntr', True)
            self.txDataRate = self._paramValue('dr', True)
            self.uplinkFreq = self._paramValue('freq', True)
            self.txPower = self._paramValue('pow', True)
            self.channelMask = self._paramValue('chmask', True)
            self.rxCounter = self._paramValue('cntr', True, True, 30)
            self.rxWindow = self._paramValue('window', True, True)
            self.rxDataRate = self._paramValue('dr', True, True)
            self.rssi = self._paramValue('rssi', True, True, units=' dBm')
            self.snr = self._paramValue('snr', True, True, units=' dB')
            self.macCommand = self._paramValue('command')
    
    sendResponse = SendResponseClass()

    class ReceivedDataClass:
        def __init__(self):
            pass

        # def __str__(self):
        #     return self._resp_list

        def stringParsing(self, string):
            self._parsed_string = string.split()
            self.port = self._parsed_string[1]
            self.payload = self._parsed_string[2]
            self.counter = self._parsed_string[5]
            self.data_rate = self._parsed_string[11]
            self.rssi = self._parsed_string[14]
            self.snr = self._parsed_string[17]
            return self._parsed_string
    
    receivedData = ReceivedDataClass()

    def receiveData(self):
        response = ''
        time.sleep(1)
        response = self.readUntil("rx ok", timeout = None)
        self.LoRaPrint(response)
        parsed = self.receivedData.stringParsing(response)
        
        return response

    """ LoRa Network Management """

    @property
    def loraClass(self):
        self.sendCommand("get_class")
        return self.readString().upper()
    @loraClass.setter
    def loraClass(self, lora_class):
        if lora_class == CLASS_A:
            self.sendCommand("set_class a")
        elif lora_class == CLASS_C:
            self.sendCommand("set_class c")
        else:
            GarudaError.selectionError("Class", "CLASS_A or CLASS_C")
        self.readUntil("ok")
        return OK

    @property
    def ADR(self):
        self.sendCommand("get_adr")
        adaptiveDR = self.readString()
        if "on" in adaptiveDR :
            return ON
        if "off" in adaptiveDR :
            return OFF
    @ADR.setter
    def ADR(self, adaptiveDR):
        if adaptiveDR == ON :
            self.sendCommand("set_adr on")
        elif adaptiveDR == OFF :
            self.sendCommand("set_adr off")
        else :
            GarudaError.selectionError("ADR state", "ON/OFF")
            return WRONG_PARAMETER
        self.readUntil("ok")
        return OK

    @property
    def dataRate(self):
        self.sendCommand("get_datarate")
        return int(self.readString())
    @dataRate.setter
    def dataRate(self, data_rate):
        data_rate = int(data_rate)
        if data_rate < 0 or data_rate > 5:
            GarudaError.rangeError("Data rate", 0, 5)
        else:
            self.sendCommand("set_dr", data_rate)
            self.readUntil("ok")
        return OK

    @property
    def rx2(self):
        self.sendCommand("get_rx2")
        return self.readString()
    @rx2.setter
    def rx2(self, data_rate, channel_freq):
        if data_rate < 0 or data_rate > 5:
            GarudaError.rangeError("Data rate", 0, 5)
        elif channel_freq < 920000000 or channel_freq > 923000000:
            GarudaError.rangeError("Frequency restricted. It", "920.0", "923.0", "MHz")
        else:
            self.sendCommand("set_rx2", data_rate, channel_freq)
            self.readUntil("ok")
        return OK

    @property
    def txPower(self):
        self.sendCommand("get_txpower")
        return self.readString()
    @txPower.setter
    def txPower(self, tx_power):
        if tx_power < 2 or tx_power > 16:
            GarudaError.rangeError("Tx power", 2, 16)
        elif tx_power % 2 != 0:
            GarudaError.selectionError("Tx power", "even number")
        else:
            self.sendCommand("set_txpower", tx_power)
            self.readUntil("ok")
        return OK

    @property
    def powerIndex(self):
        self.sendCommand("get_pwrindex")
        return self.readString()
    @powerIndex.setter
    def powerIndex(self, power_index):
        if power_index < 0 or power_index > 7:
            GarudaError.rangeError("Power index", 0, 7)
        else:
            self.sendCommand("set_pwrindex", power_index)
            self.readUntil("ok")
        return OK

    def getChannelFreq(self, channel_id):
        if channel_id < 0 or channel_id > 15:
            GarudaError.rangeError("Channel number", 0, 15)
        else:
            self.sendCommand("get_chfreq", channel_id)
            self.readUntil("ok")
        return int(self.readString())
    def setChannelFreq(self, channel_id, channel_freq):
        if channel_id < 0 or channel_id > 15:
            GarudaError.rangeError("Channel number", 0, 15)
        elif channel_freq < 920000000 or channel_freq > 923000000:
            GarudaError.rangeError("Frequency restricted. It", "920.0", "923.0", "MHz")
        else:
            self.sendCommand("set_chfreq", channel_id, channel_freq)
            self.readUntil("ok")
        return OK

    def getJoinChannelStat(self, channel_id):
        if channel_id < 0 or channel_id > 15:
            GarudaError.rangeError("Channel number", 0, 15)
        else:
            self.sendCommand("get_defchstat", channel_id)
            self.readUntil("ok")
        return self.readString()
    def setJoinChannelStat(self, channel_id, channel_state):
        if channel_id < 0 or channel_id > 15:
            GarudaError.rangeError("Channel number", 0, 15)
        elif channel_state == ON:
            self.sendCommand("set_defchstat", channel_id, "on")
        elif channel_state == OFF:
            self.sendCommand("set_defchstat", channel_id, "off")
        else:
            raise GarudaError.selectionError("Channel state", "\'ON\' or \'OFF\'")
        self.readUntil("ok")
        return OK

    def getJoinChannelMask(self):
        self.sendCommand("get_defchmask")
        return self.readString()

    def getChannelStat(self, channel_id):
        if channel_id < 0 or channel_id > 15:
            GarudaError.rangeError("Channel number", 0, 15)
        else:
            self.sendCommand("get_chstat", channel_id)
            self.readUntil("ok")
        return self.readString()
    def setChannelStat(self, channel_id, chstate):
        if channel_id < 0 or channel_id > 15:
            GarudaError.rangeError("Channel number", 0, 15)
        elif chstate == ON:
            self.sendCommand("set_chstat", channel_id, "on")
        elif chstate == OFF:
            self.sendCommand("set_chstat", channel_id, "off")
        else:
            raise GarudaError.selectionError("Channel state", "\'ON\' or \'OFF\'")
        return OK

    def getChannelMask(self):
        self.sendCommand("get_chmask")
        return self.readString()

    def getChannelDRRange(self, channel_id):
        if channel_id < 0 or channel_id > 15:
            GarudaError.rangeError("Channel number", 0, 15)
        else:
            self.sendCommand("get_chdr", channel_id)
            self.readUntil("ok")
        return self.readString()
    def setChannelDRRange(self, channel_id, dr_min, dr_max):
        if (channel_id < 0 or channel_id > 15):
            raise GarudaError.rangeError("Channel number", 0, 15)
        elif (dr_min < 0 or dr_min > 5  or dr_max < 0 or dr_max > 5):
            raise GarudaError.rangeError("Data rate", 0, 5)
        elif dr_min < dr_max:
            raise GarudaError.runtimeError("DR Min must not be greater than DR Max!")
        else:
            self.sendCommand("set_chdr", channel_id, dr_min, dr_max)
            self.readUntil("ok")
        return OK

    """ Command for Testing """

    def startTxTest():
        self.sendCommand("start_tx_test")
        self.readUntil("ok")
        return OK
    
    def stopTxTest():
        self.sendCommand("stop_tx_test")
        self.readUntil("ok")
        return OK

if __name__ == "__main__":

    GarudaShield = GarudaL76G()

    GarudaShield.info()
    # print(GarudaShield.rx2)
    print(GarudaShield.txPower)
    GarudaShield.txPower = 14
    print(GarudaShield.txPower)
    print(GarudaShield.setChannelStat(4, OFF))

    # GarudaShield.debugMode.set()
    # print(GarudaShield.debugMode.get())
