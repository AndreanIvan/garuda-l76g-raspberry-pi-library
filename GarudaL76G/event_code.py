""" Command Return Code """
" General "
OFF                     = 0
ON                      = 1
OK                      = 2
READ_TIMED_OUT          = 3
COMMAND_ERROR           = 4
" LoRa Network "
JOIN_OTAA_SUCCESS       = 101
JOIN_ABP_SUCCESS        = 102
JOIN_FAILED             = 103
JOIN_TIMED_OUT          = 104
JOIN_DC_RESTRICTED      = 105
DATA_SENT               = 106
DATA_SENT_CONFIRMED     = 107
DATA_SENT_UNCONFIRMED   = 108
SEND_RESP_TIMED_OUT     = 109
" Library Code "
GENERAL_ERROR           = 200
WRONG_PARAMETER         = 201
PARAM_OUT_OF_RANGE      = 202
PARAM_DIGIT_ERROR       = 203
NETWORK_NOT_JOINED      = 204
DEVICE_BUSY             = 205
""" Other Code """
" Class Type "
CLASS_A                 = 910
CLASS_B                 = 911
CLASS_C                 = 912
" Join Mode "
OTAA                    = 920
ABP                     = 921
" Input Mode "
AUTO                    = 930
MANUAL                  = 931
" Join Mode "
OTAA                    = 940
ABP                     = 941

code_dict = {
    OK                    : "OK",
    READ_TIMED_OUT        : "READ_TIMED_OUT",
    COMMAND_ERROR         : "COMMAND_ERROR",
    JOIN_OTAA_SUCCESS     : "JOIN_OTAA_SUCCESS",
    JOIN_ABP_SUCCESS      : "JOIN_ABP_SUCCESS",
    JOIN_FAILED           : "JOIN_FAILED",
    JOIN_TIMED_OUT        : "JOIN_TIMED_OUT",
    JOIN_DC_RESTRICTED    : "JOIN_DC_RESTRICTED",
    DATA_SENT_CONFIRMED   : "DATA_SENT_CONFIRMED",
    DATA_SENT_UNCONFIRMED : "DATA_SENT_UNCONFIRMED",
    SEND_RESP_TIMED_OUT   : "SEND_RESP_TIMED_OUT",
    GENERAL_ERROR         : "GENERAL_ERROR",
    WRONG_PARAMETER       : "WRONG_PARAMETER",
    PARAM_OUT_OF_RANGE    : "PARAM_OUT_OF_RANGE",
    PARAM_DIGIT_ERROR     : "PARAM_DIGIT_ERROR",
    NETWORK_NOT_JOINED    : "NETWORK_NOT_JOINED",
    DEVICE_BUSY           : "DEVICE_BUSY",
    OFF                   : "OFF",
    ON                    : "ON",
    CLASS_A               : "CLASS_A",
    CLASS_B               : "CLASS_B",
    CLASS_C               : "CLASS_C",
    OTAA                  : "OTAA",
    ABP                   : "ABP",
    AUTO                  : "AUTO",
    MANUAL                : "MANUAL",
    OTAA                  : "OTAA",
    ABP                   : "ABP"
}

def printCode(code):
    return code_dict[code]