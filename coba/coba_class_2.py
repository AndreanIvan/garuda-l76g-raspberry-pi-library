class Outer(object):

    def __init__(self):
        self.outer_var = "asdew"

    def get_inner_object(self):
        return self.Inner(self)

    class Inner(object):
        def __init__(self, outer):
            self.outer = outer

        @property
        def inner_var(self):
            return self.outer.outer_var

outer_object = Outer()
inner_object = outer_object.get_inner_object()

print(inner_object.inner_var)