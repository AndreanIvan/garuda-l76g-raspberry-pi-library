from coba_convert_hex import *

def func(*args):
    for elem in args:
        if type(elem) == int:
            print(elem, " : ", end='')
            print(intToHex(elem))
        if type(elem) == str:
            print(elem, " : ", end='')
            print(strToHex(elem))
    
a = 30
b = "Hello"

func(a,b)