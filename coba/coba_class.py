
class Parent:

    param1 = ""
    
    def __init__(self, param1 = ""):
        print("This is Parent")
        self.param1 = "Hello"

    def get(self, p1 = ""):
        return self.param1

    def set(self, p1 = '', p2 = ''):
        self.param1 = p1
        print(self.param1, " set to param1")
   
class Child(Parent):

    def __init__(self):
        super().__init__(self)
        print("This is Child")

class Outer:
    class Inner:
        def set(self, par1):
            self.var = par1
            return 1
        
        def get(self):
            return self.var
    
    class InnerPrivate:
        def set(self, par1):
            self.__private1 = par1
            return 1
        
        def get(self):
            return super().__private1

    InnerObj1 = Inner()
    InnerObj2 = Inner()
    InnerObj3 = InnerPrivate()

    __private1 = 0
    __private2 = ""

OuterObj = Outer()

x = Child()

while 1:
    ins1 = input("Input 1: ")
    ins2 = input("Input 2: ")
    ins3 = input("Input 3: ")
    OuterObj.InnerObj1.set(ins1)
    OuterObj.InnerObj2.set(ins2)
    OuterObj.InnerObj3.set(ins3)

    print("Value =", OuterObj.InnerObj1.get())
    print("Value =", OuterObj.InnerObj2.get())
    print("Value =", OuterObj.InnerObj3.get())