from enum_class import *
import serial
import time

""" Log Setting """
PROCESS_LOG = ON
ERROR_LOG   = ON
UART_LOG    = ON
LORA_LOG    = ON

class GarudaL76G:
    def __init__(self):
        self.Garuda_Serial = serial.Serial(
            port = '/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
            baudrate = 115200,
            parity = serial.PARITY_NONE,
            stopbits = serial.STOPBITS_ONE,
            bytesize = serial.EIGHTBITS,
            timeout = None
        )

    def UARTPrint(self, strPrint):
        if UART_LOG:
            print(strPrint)
    
    def LoRaPrint(self, strPrint):
        if LoRa_LOG:
            print(strPrint)

    def sendCommand(self, dataString = ""):
        self.Garuda_Serial.reset_input_buffer()
        dataString = 'mac ' + dataString + '\r\n'
        self.Garuda_Serial.write(dataString.encode())
        self.UARTPrint(dataString)

    def readUntilTimeout(self):
        buff = ''
        stringRead = ""
        buff = self.Garuda_Serial.read(1000)
        if buff != b'' :
            stringRead += buff.decode()
        self.UARTPrint(stringRead)
        return stringRead

    def readString(self):
        buff = ''
        stringRead = ""
        buff = self.Garuda_Serial.readline()
        if buff != b'' :
            stringRead += buff.decode()
        self.UARTPrint(stringRead)
        return stringRead

    def readUntil(self, subs = ""):
        buff = ""
        stringRead = ""
        while subs not in stringRead :
            buff = self.Garuda_Serial.read()
            stringRead += buff.decode()
        self.UARTPrint(stringRead)
        return stringRead
    
    @property
    def debugMode(self):
        self.sendCommand("get_debug")
        return self.readString()
    
    @debugMode.setter
    def debugMode(self, DebugStatus):
        if DebugStatus == ON :
            self.sendCommand("mac set_debug on")
        elif DebugStatus == OFF :
            self.sendCommand("mac set_debug off")
        else :
            print("Debug state selection must be on/off!")
            return WRONG_PARAMETER
        self.readUntil("ok")
        return OK

    def setDebug(self, DebugStatus):
        if DebugStatus == ON :
            self.sendCommand("mac set_debug on")
        elif DebugStatus == OFF :
            self.sendCommand("mac set_debug off")
        else :
            print("Debug state selection must be on/off!")
            return WRONG_PARAMETER
        
        self.readUntil("ok")
        return OK

    def getDebug(self):
        self.sendCommand("mac get_debug")
        return self.readString()

    def setADR(self, ADR):
        if ADR == ON :
            self.sendCommand("mac set_adr on")
        elif ADR == OFF :
            self.sendCommand("mac set_adr off")
        else :
            print("ADR state selection must be on/off!")
            return WRONG_PARAMETER
        self.readUntil("ok")
        return OK 

    def setTxConfirm(self, confirmStatus):
        if confirmStatus == ON :
            self.sendCommand("mac set_txconfirm on")
        elif confirmStatus == OFF :
            self.sendCommand("mac set_txconfirm off")
        else :
            print("Tx Confirm state selection must be on/off!")
            return WRONG_PARAMETER
        self.readUntil("ok")
        return OK 
    def setJoinMode(self, joinMode):
        if joinMode == OTAA :
            self.sendCommand("mac set_mode otaa")
        elif joinMode == ABP :
            self.sendCommand("mac set_mode abp")
        else :
            print("Join mode selection must be OTAA or ABP!")
            return WRONG_PARAMETER
        self.readUntil("ok")
        return OK
    def setDevEUI(self, devEUI):
        self.sendCommand("mac set_deveui " + devEUI)
        return OK
    def setAppEUI(self, appEUI):
        self.sendCommand("mac set_appeui " + appEUI)
        return OK
    def setAppKey(self, appKey):
        self.sendCommand("mac set_appkey " + appKey)
        return OK
    def join(self):
        self.sendCommand("mac join")
        self.readString()
        self.readUntil("joined")
        return OK


    def getTxConfirm(self):
        self.sendCommand("mac get_txconfirm")
        return self.readString()
    def getJoinMode(self):
        self.sendCommand("mac get_mode")
        return self.readString()
    def getDevEUI(self):
        self.sendCommand("mac get_deveui")
        return self.readString()
    def getAppEUI(self):
        self.sendCommand("mac get_appeui")
        return self.readString()
    def getAppKey(self):
        self.sendCommand("mac get_appkey")
        return self.readString()
    
    # debugMode.def

GarudaShield = GarudaL76G()

GarudaShield.debugMode.set()
print(GarudaShield.debugMode.get())
