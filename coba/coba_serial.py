#!/usr/bin/env python3
import serial
import time

ser = serial.Serial(
        port = '/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        baudrate = 115200,
        parity = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        timeout = None
)
ser.flush()

counter=0

def get_response(self, timeout=None):
        """Get response from module.

        This is a blocking call: it will return a response line or raise
        Rak811TimeoutError if a response line is not received in time.
        """
        if timeout is None:
            timeout = self._response_timeout

        with self._cv_serial:
            while len(self._read_buffer) == 0:
                success = self._cv_serial.wait(timeout)
                if not success:
                    raise Rak811TimeoutError(
                        'Timeout while waiting for response'
                    )
            response = self._read_buffer.pop(0)
        return response

while 1:
        # ser.write(b'Write counter: %d \n'%(counter))
        # time.sleep(1)
        # counter += 1
        ins = input("Command: ") + '\n'
        print(type(ins))

        ins_bytes = str.encode(ins)
        print(type(ins_bytes))
        
        ser.write(ins.encode())

        x = get_response()
        print(x)
        