#!/usr/bin/env python3
import serial

ser = serial.Serial(
    port = '/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
    baudrate = 115200,
    parity = serial.PARITY_NONE,
    stopbits = serial.STOPBITS_ONE,
    bytesize = serial.EIGHTBITS,
    timeout = None
)

while 1:
    # ser.reset_input_buffer()
    buff = ser.readline()
    stringRead = buff.decode()
    print(stringRead, end='')