#!/usr/bin/env python3
import serial
import time

PROCESS_LOG = 1
ERROR_LOG   = 1
UART_LOG    = 1
LORA_LOG    = 1

Garuda_Serial = serial.Serial(
    port = '/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
    baudrate = 115200,
    parity = serial.PARITY_NONE,
    stopbits = serial.STOPBITS_ONE,
    bytesize = serial.EIGHTBITS,
    timeout = None
)

def UARTPrint(strPrint):
    if UART_LOG:
        print(strPrint)

def sendCommand(*args):
    Garuda_Serial.reset_input_buffer()
    dataString = ''
    for arg in args:
        dataString += str(arg)
        if arg != args[len(args) - 1]:
            dataString += ' '
        
    dataString = 'mac ' + dataString + '\r\n'
    Garuda_Serial.write(dataString.encode())
    UARTPrint(dataString)

def readString(self):
    buff = ''
    stringRead = ""
    buff = Garuda_Serial.readline()
    stringRead += buff.decode()
    UARTPrint(stringRead)
    return stringRead

def readUntil(string = "", timeout = 1):
    stringRead = ""
    # If there's no argument, read until input buffer is empty
    if string is "":
        time.sleep(0.1)
        while Garuda_Serial.in_waiting > 0 :
            buff = Garuda_Serial.readline()
            stringRead += buff.decode()
    # If timeout is set to None, read until string found only, regardless the timeout
    else:
        isFound = False
        if timeout != None :
            timeout += time.time()
        while not isFound:
            buff = Garuda_Serial.readline()
            stringRead += buff.decode()
            if string in stringRead:
                isFound = True
            if timeout != None and time.time() > timeout:
                print("Read timeout!")
                UARTPrint(stringRead)
                return stringRead
    UARTPrint(stringRead)
    return stringRead
   
def writeSerial(dataString = ""):

    dataString += '\r\n'

    Garuda_Serial.write(dataString.encode())

counter=0
buff = ''
x = ''

while 1:
    # while Garuda_Serial.in_waiting > 0:
    #     resp = readString()
    Garuda_Serial.reset_input_buffer()
    Garuda_Serial.reset_output_buffer()

    ins = input("Write command: ")
    writeSerial(ins)
    
    if "get_info" in ins:        
        resp = readUntil("debug")
    elif "mac join" in ins:
        resp = readUntil("joined")
    # elif "mac send" in ins:
    #     resp = readUntil("rx ok")
    else :
        resp = readUntil()
        resp2 = resp.split()
        count = 0
        print(resp2)
        for elem in resp2:
            print(count, ':', elem)
            count += 1
        # print(resp)

    print("================================")
    time.sleep(1)