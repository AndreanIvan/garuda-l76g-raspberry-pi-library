ON = 0
OFF = 1
OTAA = 2
ABP = 3
AUTO = 4
MANUAL = 5
CLASS_A = 6
CLASS_B = 7
CLASS_C = 8

printCode = {
    ON : "ON",
    OFF : "OFF",
    OTAA : "OTAA",
    ABP : "ABP",
    AUTO : "AUTO",
    MANUAL : "MANUAL",
    CLASS_A : "CLASS_A",
    CLASS_B : "CLASS_B",
    CLASS_C : "CLASS_C"
}

def functionCode(code):
    return printCode[code]


print(functionCode(ON))