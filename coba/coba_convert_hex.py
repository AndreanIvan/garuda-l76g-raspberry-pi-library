# Type conversion, INT to HEX, STR to HEX

def intToHex(number):
    hexnum = str(hex(number).lstrip('0x'))
    if len(hexnum) % 2 != 0: hexnum = '0' + hexnum
    return hexnum

def strToHex(strnum):
    strhex = ""
    for letter in strnum:
        strhex += hex(ord(letter)).lstrip('0x')
    return strhex

number = 4829485
string = "Hello World!"

if __name__ == "__main__":
    print(intToHex(number))
    print(strToHex(string))