from enum_class import *
import serial
import time

""" Log Setting """
PROCESS_LOG = ON
ERROR_LOG   = ON
UART_LOG    = ON
LORA_LOG    = ON

class GarudaL76G:
    def __init__(self):
        pass
    
    # def createInner(self):
    #     return self.ParamAccess(self)

    class ParamAccess:
        def __init__(self, command = "", min_val = -1, max_val = -1, channel = 0):
            self.Garuda_Serial = serial.Serial(
                port = '/dev/ttyS0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
                baudrate = 115200,
                parity = serial.PARITY_NONE,
                stopbits = serial.STOPBITS_ONE,
                bytesize = serial.EIGHTBITS,
                timeout = None
            )
            self.min_val = min_val
            self.max_val = max_val
            self.channel = channel
            self.getCommand = "mac get_" + command
            self.setCommand = "mac set_" + command

        def UARTPrint(self, strPrint):
            if UART_LOG:
                print(strPrint)
        
        def LoRaPrint(self, strPrint):
            if LoRa_LOG:
                print(strPrint)

        def writeSerial(self, dataString = ""):
            self.Garuda_Serial.reset_input_buffer()
            dataString += '\r\n'
            self.Garuda_Serial.write(dataString.encode())
            self.UARTPrint(dataString)

        def readUntilTimeout(self):
            buff = ''
            stringRead = ""
            buff = self.Garuda_Serial.read(1000)
            if buff != b'' :
                stringRead += buff.decode()
            self.UARTPrint(stringRead)
            return stringRead

        def readString(self):
            buff = ''
            stringRead = ""
            buff = self.Garuda_Serial.readline()
            if buff != b'' :
                stringRead += buff.decode()
            self.UARTPrint(stringRead)
            return stringRead

        def readUntil(self, subs = ""):
            buff = ""
            stringRead = ""
            while subs not in stringRead :
                buff = self.Garuda_Serial.read()
                stringRead += buff.decode()
            self.UARTPrint(stringRead)
            return stringRead

        def get(self, *args):
            # self.getCommand = "mac"
            for param in args:
                self.getCommand += ' ' + param
            self.writeSerial(self.getCommand)
            return self.readString()
        
        def set(self, *args):
            # self.setCommand = "mac"
            for param in args:
                self.getCommand += ' ' + param
            self.writeSerial(self.getCommand)
            return OK

    debugMode = ParamAccess("debug")
    # debugMode.def

GarudaShield = GarudaL76G()

GarudaShield.debugMode.set("on")
print(GarudaShield.debugMode.get())
